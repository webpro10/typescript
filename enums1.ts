enum CadinaDirections {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let currentDirection = CadinaDirections.East;

console.log(currentDirection);