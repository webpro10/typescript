type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel,
}

const CarYear: CarYear = 2001;
const CarType: CarType = "toyota";
const CarModel: CarModel = "Corolla";

const car1: Car = {
    year: 2001,
    type: "Nissan GT-R35",
    model: "XXX",
}
console.log(car1)